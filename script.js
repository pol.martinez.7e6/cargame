let carretera;
let carreteraY = 0;
let carreteraSpeed = 5;
let carX = 300;
let carY = 500;
let carSpeed = 5;
let cars = [];
let puntos = 0;
let gameOver = false;

function setup() {
  const cv = createCanvas(800, 690);
  cv.parent("canvas");
  carretera = loadImage("carretera.jpg");
  car = loadImage("car.webp");
  carOpp = loadImage("carOpp.webp");
}

function draw() {
  if (!gameOver) {
    // Pintar la carretera
    image(carretera, 0, carreteraY, width, height);
    image(carretera, 0, carreteraY - height, width, height);

    // Mover la carretera
    carreteraY += carreteraSpeed;

    // Si la carretera se ha movido una altura igual a su tamaño, volverla a la posición inicial
    if (carreteraY >= height) {
      carreteraY = 0;
    }

    // Mover el coche del jugador con el ratón
    if (mouseX > carX + carSpeed) {
      carX += carSpeed;
    } else if (mouseX < carX - carSpeed) {
      carX -= carSpeed;
    }

    // Pintar el coche del jugador
    image(car, carX, carY, width / 7, height / 4);

    // Generar coches enemigos
    if (frameCount % 80 === 0) {
      cars.push({
        x: random(100, width - 100),
        y: -100,
        speed: random(2, 5),
      });
    }

    // Pintar coches enemigos
    for (let i = 0; i < cars.length; i++) {
      image(carOpp, cars[i].x, cars[i].y, width / 7, height / 4);
      cars[i].y += cars[i].speed;

      // Comprobar colisión
      if (
        cars[i].y + height / 4 > carY &&
        cars[i].y < carY + height / 4 &&
        cars[i].x + width / 7 > carX &&
        cars[i].x < carX + width / 7
      ) {
        gameOver = true;
      }

      // Eliminar coches que salen de la pantalla
      if (cars[i].y > height) {
        cars.splice(i, 1);
        puntos++;
      }
    }

    // Mostrar puntuación
    fill(255);
    textSize(32);
    text(`Puntos: ${puntos}`, 20, 40);
  } else {
    // Mostrar mensaje de Game Over
    fill(255);
    textSize(64);
    text("GAME OVER", width / 2 - 200, height / 2);
    textSize(32);
    text(`Puntos: ${puntos}`, width / 2 - 50, height / 2 + 50);
  }
}